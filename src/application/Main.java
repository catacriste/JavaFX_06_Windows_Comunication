package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;


/*Comunica��o entre janelas (principal e uma modal)
 * 
 * 
 * -Neste exemplo vamos criar uma ConfirmationBox, modal, depende da janela principal,
 * que devolver� uma resposta para a janela principal
 * 
 * - passo 1 - Criar uma nova janela Modal ConfirmationBox(title, msg)
 * 				Recebe 2 strings, para o titulo e para a mensagem
 * 				Devolve um boolean com a resposta.
 * - passo 2 - Na main, criamos os objetos gr�ficos habituais e chamamos a alertBox
 * 				a partir da a��o de um bot�o. Contudo temos que ter um boolean para receber para receber a resposta
 * 				a resposta (true ou false) que a vai ser devolvido. De seguida, com um IF,
 * 				chamamos a AlertBox, para mostrar a resposta.
 * */
import javafx.scene.layout.StackPane;

public class Main extends Application {
	Button btnAlert;
//Cria Bot�o null, fora dos m�todos para ser acess�vel a todos
	@Override
	public void start(Stage primaryStage) {
		try {
		
			btnAlert = new Button("Clica-me");						//Inicializa��o
			btnAlert.setOnAction(e-> {
				if(Utils.confirmationBox("Retarded", "Sim ou sim ?"))
				{
					Utils.alertbox("AlertBox", "Vou fechar esta janela");
					primaryStage.close();
				}
				else {
					Utils.alertbox("AlertBox", "N�o fecha a janela");
				}
				
			});
			
			StackPane layoutRoot = new StackPane(); //layout Principal
			layoutRoot.getChildren().add(btnAlert);
			
			Scene scene = new Scene(layoutRoot, 400,600);
			
			primaryStage.setScene(scene);
			primaryStage.setTitle("Janela Modal");
			primaryStage.show();
			
			
			
			
			
			
			
		} catch(Exception e) {					//Tratamento Gen�rico das Exe��es
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		launch(args);
	}
}
